import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  private name: string;
  private img_url: string;
  constructor(
    private scanner: BarcodeScanner,
    private httpClient: HttpClient
  ) {}

  scan() {
    this.scanner.scan().then(data => {
      let url = 'https://fr.openfoodfacts.org/api/v0/produit/';
      this.httpClient.get(url + data.text)
        .subscribe(json => {
          this.name = json.product.product_name_fr;
          this.img_url = json.product.image_front_thumb_url;
        });
    });
  }

}
