interface Product{
    name: string;
    checked: boolean;
    quantity: number;
}