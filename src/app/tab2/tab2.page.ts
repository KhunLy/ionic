import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  private productName: string;
  private quantity: number = 1;

  private productsList: Array<Product>;
  constructor(
    private actCtrl: ActionSheetController,
    private storage: Storage
    ) {
    this.storage.get('cart').then((data) => {
      this.productsList = data || [];
    });
  }

  addProduct() {
    this.productsList.push({
      name: this.productName,
      checked: false,
      quantity: this.quantity
    });
    this.productName = null;
    this.quantity = 1;
    this.save();
  }

  displayActions(item: Product) {
    this.actCtrl.create({
      buttons: [{
        text: 'Delete',
        handler: () => {
          // let index = this.productsList.indexOf(item);
          // this.productsList.splice(index, 1);
          this.productsList 
            = this.productsList.filter(p => p != item);
          this.save();
        }
      },{
        text: 'Check',
        handler: () => {
          item.checked = !item.checked;
          this.save();
        }
      },{
        text: 'Cancel'
      }]
    }).then(ac => ac.present());
  }
  save() {
    this.storage.set('cart', this.productsList);
  }
}
